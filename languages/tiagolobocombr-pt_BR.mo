��    a      $  �   ,      8     9  	   K     U     c     y     �  
   �     �     �     �     �     	     	  
   "	     -	     :	  �   @	     �	     �	     �	     �	     �	     �	  
   
     
     %
     1
     @
     W
  
   m
     x
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          (     :     T     ]     k     �     �     �     �     �  
   �  	   �     �  	   
          '     :     Z     h     ~     �     �     �     �     �     �     �     �     �     �     �  	                  )     2     A     H     K     Z     _     e     q     ~     �     �     �  
   �     �     �     	          $     -  q  3     �     �     �      �                7     G     c     p     �     �     �     �     �     �  �   �     u     |     �     �     �     �     �  	   �     �     �     	     '     C     P     i     |     �  #   �     �     �     �  
   �               !     :     K     h     q     �     �     �  #   �  *   �     #     )  
   ;     F     ]     m     �     �  
   �     �     �     �     �     �     �     �          '     7     T     o       	   �  
   �     �  
   �     �     �     �     �     �     �     �     
          6  #   N     r  	   �  
   �     �     �     �     �     �     E   7   0          4   `           3                     D   C   B   \   F   P      .   K   ;       L   A              ]   R       
      @   "       W          8   Y                 $   )   O                     I   Z   9   ,   !   =   ?   6   #   S   1      ^         :   2       U              M      5   &       N   H       *          G               T   <       +                    >      /   _   [   X       %   	          '   (   a          -   Q                       V          J    Add Another Slide Add Image Add New Combo Add New Internet Plan Add new _combo Add new _internet plan All Combos All Internet Plans Background Color Background Image (Desktop) Background Image (Mobile) Button CSS Classes Button Text Button URL CSS Gradient Cents Colors: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Types: wedo-c-button--solid / wedo-c-button--big Combo Combo Archives Combo Attributes Combos Combos list Combos list navigation Conditions Content Description Desktop Combos Desktop Internet Plans Display on front page Edit Combo Edit Internet Plan Featured Image Featured plan Filter Combos list Filter Internet Plans list Footer Form Shortcode From Header Hero Image Inner (Desktop) Image Inner (Mobile) Insert into combo Insert into internet plan Internet Internet Plan Internet Plan Archives Internet Plan Attributes Internet Plans Internet Plans list Internet Plans list navigation Items Label text New Combo New Internet Plan Not found Not found in Trash Number of channels PNG with transparent background Parent Combo: Parent Internet Plan: Period Phone Plan Prefix Price Remove Slide Remove featured image Search Combo Search Internet Plan Set featured image Settings Show on plan Slide {#} Speed Subscribe now Subtitle Subtitle Color Suffix TV Telesales Text Text Title Title Color Update Combo Update Internet Plan Uploaded to this combo Uploaded to this internet plan Use as featured image View Combo View Combos View Internet Plan View Internet Plans combos internet month Project-Id-Version: 
PO-Revision-Date: 2020-07-15 05:09-0300
Last-Translator: 
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __
X-Poedit-SearchPath-0: desktopnovainternetcombr.php
 Adicionar Novo Slide Adicionar imagem Adicionar Novo Combo Adicionar Novo Plano de Internet Adicionar novo combo Adicionar novo plano de internet Todos os Combos Todos os Planos de Internet Cor de fundo Imagem de fundo (Desktop) Imagem de fundo (Mobile) Classes de CSS do Botão Texto do Botão URL do Botão CSS do Gradiente Centavos Cores: wedo-c-button--primary, wedo-c-button--secondary, wedo-c-button--white / Tipos: wedo-c-button--solid / wedo-c-button--big Combos Arquivo de Combos Atributos de combos Combos Lista de Combos Navegação na lista de Combos Condições Conteúdo Descrição Desktop Combos Planos de Internet da Desktop Mostrar na primeira página Editar Combo Editar Plano de Internet Imagem em destaque Plano em destaque Filtrar lista de Combos Filtrar lista de Planos de Internet Rodapé Shortcode do Formulário A partir de Cabeçalho Hero Imagem Interior (Desktop) Imagem Interior (Mobile) Inserir no combo Inserir em plano de internet Internet Plano de Internet Arquivo de Planos de Internet Atributos de planos de internet Planos de Internet Filtrar lista de Planos de Internet Navegação na lista de Planos de Internet Itens Texto da Etiqueta Novo Combo Novo Plano de Internet Não encontrado Não encontrado na lixeira Número de canais PNG com fundo transparente Combo Pai: Plano de Internet Pai: Período Telefone Plano Prefixo Preço Remover Slide Remover imagem em destaque Pesquisar Combo Pesquisar Planos de Internet Definir imagem em destaque Configurações Mostrar no plano Slide {#} Velocidade Assine agora Subtítulo Cor do Subtítulo Sufixo TV Texto da Televendas Texto Título Cor do Título Atualizar Combo Atualizar Plano de Internet Enviado para este combo Enviado para este plano de internet Usar como imagem destacada Ver Combo Ver Combos Ver Plano de Internet Ver Planos de Internet combos internet mês 