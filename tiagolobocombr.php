<?php
/**
 * Tiago Lobo - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   TiagoLobo
 * @author    WeDo Digital <contato@wedodigital.com.br>
 * @copyright 2020 WeDo Digital
 * @license   Proprietary https://wedodigital.com.br
 * @link      https://wedodigital.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Tiago Lobo - mu-plugin
 * Plugin URI:  https://wedodigital.com.br
 * Description: Customizations for tiagolobo.com.br site
 * Version:     1.0.1
 * Author:      WeDo Digital
 * Author URI:  https://wedodigital.com.br/
 * Text Domain: tiagolobocombr
 * License:     Proprietary
 * License URI: https://wedodigital.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('tiagolobocombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Register post types
 * ********************************************************************************/

/**
 * Plano de Governo
 */

function cpt_tiagolobocombr_planodegoverno() 
{
    $labels = array(
            'name'                  => __('Plano de Governo', 'tiagolobocombr'),
            'singular_name'         => __('Plano de Governo', 'tiagolobocombr'),
            'menu_name'             => __('Plano de Governo', 'tiagolobocombr'),
            'name_admin_bar'        => __('Plano de Governo', 'tiagolobocombr'),
            'archives'              => __('Plano de Governo Archives', 'tiagolobocombr'),
            'attributes'            => __('Plano de Governo Attributes', 'tiagolobocombr'),
            'parent_item_colon'     => __('Plano de Governo Pai:', 'tiagolobocombr'),
            'all_items'             => __('Todos os Plano de Governo', 'tiagolobocombr'),
            'add_new_item'          => __('Adicionar Novo Plano de Governo', 'tiagolobocombr'),
            'add_new'               => __('Adicionar novo plano de governo', 'tiagolobocombr'),
            'new_item'              => __('Novo Plano de Governo', 'tiagolobocombr'),
            'edit_item'             => __('Editar Plano de Governo', 'tiagolobocombr'),
            'update_item'           => __('Atualizar Plano de Governo', 'tiagolobocombr'),
            'view_item'             => __('Ver Plano de Governo', 'tiagolobocombr'),
            'view_items'            => __('Ver Plano de Governo', 'tiagolobocombr'),
            'search_items'          => __('Procurar Plano de Governo', 'tiagolobocombr'),
            'not_found'             => __('Not found', 'tiagolobocombr'),
            'not_found_in_trash'    => __('Not found in Trash', 'tiagolobocombr'),
            'featured_image'        => __('Featured Image', 'tiagolobocombr'),
            'set_featured_image'    => __('Set featured image', 'tiagolobocombr'),
            'remove_featured_image' => __('Remove featured image', 'tiagolobocombr'),
            'use_featured_image'    => __('Use as featured image', 'tiagolobocombr'),
            'insert_into_item'      => __('Insert into plano de governo', 'tiagolobocombr'),
            'uploaded_to_this_item' => __('Uploaded to this plano de governo', 'tiagolobocombr'),
            'items_list'            => __('Plano de Governo list', 'tiagolobocombr'),
            'items_list_navigation' => __('Plano de Governo list navigation', 'tiagolobocombr'),
            'filter_items_list'     => __('Filter Plano de Governo list', 'tiagolobocombr'),
    );
    $rewrite = array(
            'slug'                  => __('plano-de-governo', 'tiagolobocombr'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Plano de Governo', 'tiagolobocombr'),
            'description'           => __('', 'tiagolobocombr'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-media-spreadsheet',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('planodegoverno', $args); 
} 
add_action('init', 'cpt_tiagolobocombr_planodegoverno', 0);

/**
 * Vereadores
 */
function cpt_tiagolobocombr_vereadores() 
{
    $labels = array(
            'name'                  => __('Vereadores', 'tiagolobocombr'),
            'singular_name'         => __('Vereador(a)', 'tiagolobocombr'),
            'menu_name'             => __('Vereadores', 'tiagolobocombr'),
            'name_admin_bar'        => __('Vereador(a)', 'tiagolobocombr'),
            'archives'              => __('Vereador(a) Archives', 'tiagolobocombr'),
            'attributes'            => __('Vereador(a) Attributes', 'tiagolobocombr'),
            'parent_item_colon'     => __('Parent Vereador(a):', 'tiagolobocombr'),
            'all_items'             => __('All Vereadores', 'tiagolobocombr'),
            'add_new_item'          => __('Add New Vereador(a)', 'tiagolobocombr'),
            'add_new'               => __('Add new _combo', 'tiagolobocombr'),
            'new_item'              => __('New Vereador(a)', 'tiagolobocombr'),
            'edit_item'             => __('Edit Vereador(a)', 'tiagolobocombr'),
            'update_item'           => __('Update Vereador(a)', 'tiagolobocombr'),
            'view_item'             => __('View Vereador(a)', 'tiagolobocombr'),
            'view_items'            => __('View Vereadores', 'tiagolobocombr'),
            'search_items'          => __('Search Vereador(a)', 'tiagolobocombr'),
            'not_found'             => __('Not found', 'tiagolobocombr'),
            'not_found_in_trash'    => __('Not found in Trash', 'tiagolobocombr'),
            'featured_image'        => __('Featured Image', 'tiagolobocombr'),
            'set_featured_image'    => __('Set featured image', 'tiagolobocombr'),
            'remove_featured_image' => __('Remove featured image', 'tiagolobocombr'),
            'use_featured_image'    => __('Use as featured image', 'tiagolobocombr'),
            'insert_into_item'      => __('Insert into combo', 'tiagolobocombr'),
            'uploaded_to_this_item' => __('Uploaded to this combo', 'tiagolobocombr'),
            'items_list'            => __('Vereadores list', 'tiagolobocombr'),
            'items_list_navigation' => __('Vereadores list navigation', 'tiagolobocombr'),
            'filter_items_list'     => __('Filter Vereadores list', 'tiagolobocombr'),
    );
    $rewrite = array(
            'slug'                  => __('vereadores', 'tiagolobocombr'),
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
    );
    $args = array(
            'label'                 => __('Vereadores', 'tiagolobocombr'),
            'description'           => __('Vereadores', 'tiagolobocombr'),
            'labels'                => $labels,
            'supports'              => array('title', 'revisions', 'page-attributes'),
            'taxonomies'            => array(''),
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-id-alt',
            'show_in_admin_bar'     => false,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => false,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => $rewrite,
            'capability_type'       => 'page',
    );
    register_post_type('vereadores', $args); 
} 
add_action('init', 'cpt_tiagolobocombr_vereadores', 0);
